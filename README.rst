=====
Dolly
=====


.. image:: https://img.shields.io/pypi/v/dolly.svg
        :target: https://pypi.python.org/pypi/dolly

.. image:: https://img.shields.io/travis/lisael/dolly.svg
        :target: https://travis-ci.org/lisael/dolly

.. image:: https://readthedocs.org/projects/dolly/badge/?version=latest
        :target: https://dolly.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Async rREST web framework


* Free software: GNU General Public License v3
* Documentation: https://dolly.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
