import re
from .fields import Field, SerialField
from dolly.conf.setup import current_app


def tablename_from_modelname(model):
    result = ""
    for char in model:
        if char.isupper():
            result += "_" + char.lower()
        else:
            result += char
    sz = 0
    while len(result) != sz:
        sz = len(result)
        result = re.sub("_([^_]_)", r"\1", result)
    try:
        return current_app.app + "_" + result.strip("_")
    except AttributeError:
        return result.strip("_")


class _Meta:
    def __init__(self):
        self.fields = None
        self.tablename = None
        self.schema = "public"

    @property
    def ordered_fields(self):
        return sorted(self.fields.values(), key=lambda x: x.ord)

    def sql_pk_constraint(self):
        pks = ['"%s"' % k for k, v in self.fields.items() if v.pk]
        return "CONSTRAINT {}_{}_pk PRIMARY KEY ({})".format(self.schema, self.tablename, ", ".join(pks))

    def sql_constraints(self):
        constraints = [
            self.sql_pk_constraint(),
        ]
        return "\n  ".join(constraints)

    def sql_create(self):
        # import ipdb; ipdb.set_trace()
        return """
CREATE TABLE "{schema}"."{tablename}" (
  {fields});""".format(
                    schema=self.schema,
                    tablename=self.tablename,
                    fields=",\n  ".join(
                        [f.sql_create() for f in self.ordered_fields] +
                        [self.sql_constraints()]
                    )).strip()

    def sql_drop(self):
        return 'DROP TABLE "{}"."{}";'.format(self.schema, self.tablename)


class _ModelMeta(type):
    def __new__(cls, name, bases, attrs):
        if "_base_model" not in attrs:
            usermeta = attrs.get("Meta")
            meta = _Meta()
            meta.fields = {k: v for k, v in attrs.items() if isinstance(v, Field)}
            meta.tablename = tablename_from_modelname(name)
            if usermeta:
                meta.tablename = usermeta.tablename
                meta.schema = usermeta.schema
            pks = [k for k, v in meta.fields.items() if v.pk]
            if not pks:
                meta.fields["id"] = SerialField(pk=True, ord=-1, name="id")
            attrs["Meta"] = meta
        new = super().__new__(cls, name, bases, attrs)
        return new


class Model(metaclass=_ModelMeta):
    _base_model = True
    def __init__(self, **kwargs):
        try:
            self._values = kwargs["_values"]
        except KeyError:
            for k, v in kwargs:
                if k not in self.Meta.fields:
                    raise TypeError("'{}' is not a {} field.".format(
                        k, self.__class__.__name__))
                setattr(self, k, v)
