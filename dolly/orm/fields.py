class Field:
    _ord = 0

    def __init__(self, null=True, pk=False, unique=False, ord=-1, name=None):
        self.null = null
        self.pk = pk
        self.unique = unique
        self.name = name
        self.ord = ord

    def __get__(self, instance, owner):
        if instance is not None:
            return instance._values[self.fieldname]
        return self

    def __set__(self, instance, value):
        instance._values[self.fieldname] = value

    def __set_name__(self, owner, name):
        self.name = name
        self.__class__._ord += 1
        self.ord = self.__class__._ord

    def sql_create(self):
        return '"{field}" {type} {constraints}'.format(
            field=self.name,
            type=self.sql_type(),
            constraints=self.sql_constraints())

    def sql_constraints(self):
        return "" if self.null else "NOT NULL"


class SerialField(Field):
    def sql_type(self):
        return "serial"


class BooleanField(Field):
    def sql_type(self):
        return "boolean"


class TextField(Field):
    def __init__(self, size=-1, **kwargs):
        super().__init__(**kwargs)
        self.size = size

    def sql_type(self):
        return "text" if self.size < 0 else "varchar({})".format(self.size)
