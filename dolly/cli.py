# -*- coding: utf-8 -*-
import os
import sys
import warnings
import importlib
from pathlib import Path
import asyncio
from asyncio import iscoroutinefunction

import click
from colorama import Fore, Style


__cmd_name__ = os.path.basename(sys.argv[0])


CONTEXT_SETTINGS = dict(auto_envvar_prefix=__cmd_name__.upper())


class Context(object):

    def __init__(self):
        self.verbose = False
        self.settings = None


pass_context = click.make_pass_decorator(Context, ensure=True)


class Command:
    cli_config = []

    def __init__(self, verbose):
        self.verbose = verbose

    def echo(self, msg, *args, err=False, color=None):
        """Logs a message to stdout."""
        if args:
            msg %= args
        if color:
            msg = "%s%s%s" % (getattr(Fore, color.upper()), msg, Style.RESET_ALL)
        click.echo(msg, err=err)

    def err(self, msg, *args, color=None):
        "Log a message to stderr"
        self.echo(msg, *args, err=True, color=color)

    def vecho(self, msg, *args, color=None):
        """Logs a verbose message to stdout."""
        if self.verbose:
            self.echo(msg, *args, color=color)

    def success(self, msg, *args):
        """Logs a message to stdout, colored green"""
        if args:
            msg %= args
        self.echo(msg, color="green")

    def vsuccess(self, msg, *args):
        """Logs a verbose message to stdout, colored green"""
        if self.verbose:
            self.success(msg, *args)

    def warn(self, msg, *args):
        """Logs a message to stderr, colored yellow"""
        self.err(msg, *args, color="yellow")

    def vwarn(self, msg, *args):
        """Logs a message to stderr."""
        if self.verbose:
            self.warn(msg, *args)

    def error(self, msg, *args):
        """Logs a message to stderr."""
        self.err(msg, *args, color='red')

    def verror(self, msg, *args):
        """Logs a message to stderr."""
        if self.verbose:
            self.error(msg, *args)


def _modules_in_package(pkg):
    root = Path(pkg.__file__).parent
    return [child[:-3] for child in os.listdir(root)
            if not child.startswith("_")
            and child.endswith(".py")]


def _command_in_module(mod):
    cmd_cls = [v for k, v in mod.__dict__.items()
               if not k.startswith("_")
               and isinstance(v, type)
               and v is not Command
               and Command in v.__mro__][0]

    @pass_context
    def cmd(ctx, *args, **kwargs):
        if iscoroutinefunction(cmd_cls.__call__):
            asyncio.run(cmd_cls(verbose=ctx.verbose)(*args, **kwargs))
        else:
            cmd_cls(verbose=ctx.verbose)(*args, **kwargs)

    for conf in cmd_cls.cli_config:
        cmd = conf(cmd)
    cmd = click.command()(cmd)
    cmd.short_help = cmd_cls.__doc__.lstrip()
    return cmd


class DollySubCli(click.MultiCommand):
    "Sub group of commands"

    def __init__(self, *args, **kwargs):
        self.module = None
        super(DollySubCli, self).__init__(*args, **kwargs)

    def list_commands(self, ctx):
        return _modules_in_package(self.module)

    def get_command(self, ctx, name):
        mod = importlib.import_module(".".join([self.module.__name__, name]))
        return _command_in_module(mod)


class DollyCLI(click.MultiCommand):

    def __init__(self, *args, **kwargs):
        super(DollyCLI, self).__init__(*args, **kwargs)
        self.cmd_modules = {}
        from dolly import commands
        self.builtins = _modules_in_package(commands)

    def list_commands(self, ctx):
        from dolly.conf.setup import setup, app_registry
        setup()
        for name, app in app_registry.items():
            self.cmd_modules[name] = app.commands
        return self.builtins + list(app_registry.keys()) + ["help"]

    def get_command(self, ctx, name):
        if name in self.builtins:
            mod = importlib.import_module(".".join(["dolly.commands", name]))
            return _command_in_module(mod)

        if name is "help":
            cmd = click.command()(lambda: None)
            cmd.short_help = "Show help for a subcommand"
            return cmd

        if not self.cmd_modules:
            self.list_commands(ctx)

        try:
            module = self.cmd_modules[name]
        except KeyError:
            raise click.ClickException(
                "Sub command '%s' not found.\nTry `dolly --help`" % name)

        @click.command(cls=DollySubCli)
        @pass_context
        def grp(ctx):
            pass

        try:
            grp.short_help = module.__doc__.lstrip()
        except AttributeError:
            warnings.warn(
                "Add a docstring to %s to generate command help." %
                module.__file__)
        grp.module = module
        return grp


@click.command(cls=DollyCLI, context_settings=CONTEXT_SETTINGS)
@click.option('--settings', type=click.File(), help='Settings file')
@click.option('-v', '--verbose', is_flag=True,
              help='Enables verbose mode.')
@pass_context
def run(ctx, verbose, settings):
    """Dolly CLI tool"""
    ctx.verbose = verbose
    if settings is not None:
        os.environ[__cmd_name__.upper() + "_SETTINGS"] = settings


def main():
    import sys
    if sys.argv[1] == "help":
        sys.argv = sys.argv[:1] + sys.argv[2:] + ["--help"]
    run()
