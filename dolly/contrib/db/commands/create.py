import click
from asyncpg.exceptions import InvalidCatalogNameError

from dolly.cli import Command
from dolly.conf.setup import app_registry
from dolly.database import transaction, Database


class Create(Command):
    """
    Create a new database and install the schema
    """
    cli_config = [
        click.option("-r", "--recreate", default=False, is_flag=True,
                     help="Drop the database, if it exists, before re-creating."),
        click.option("-d", "--database", default="default",
                     help="Target database name."),
        click.option("-a", "--admin-database", default="admin",
                     help="Admin's database."),
    ]

    async def __call__(self, recreate: bool, database: str,
                       admin_database: str):
        db = Database.get(database)
        if recreate:
            admin = Database.get(admin_database)
            await admin.init()
            dbname = db.con_args["database"]
            async with admin.connection() as con:
                try:
                    # kill all connections
                    result = await con.execute("""
                        SELECT pg_terminate_backend(pid)
                          FROM pg_stat_activity
                          WHERE pid <> pg_backend_pid()
                          AND datname = $1;""", dbname)
                    self.vwarn(result)
                    result = await con.execute('DROP DATABASE "%s"' % dbname)
                    self.vwarn(result)
                except InvalidCatalogNameError:

                    pass
        result = await con.execute('CREATE DATABASE "%s"' % dbname)
        self.vwarn(result)
        await db.init()
        async with transaction(database) as trn:
            for app in app_registry.values():
                for cls in app.model_classes:
                    result = await trn.alter(cls.Meta.sql_create())
                    self.vwarn(result)
