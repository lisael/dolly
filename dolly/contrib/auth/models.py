from dolly.orm import models
from dolly.orm import fields


class User(models.Model):
    username = fields.TextField(null=False)
    email = fields.TextField()
