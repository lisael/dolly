import click

from dolly.cli import Command


class CreatUser(Command):
    "Create a user."
    cli_config = [
        click.argument("username"),
        click.argument("email"),
        click.argument("password"),
    ]

    def __call__(self, username, email, password):
        self.warn(f"Creating user `{username}`")
