# -*- coding: utf-8 -*-

"""Top-level package for Dolly."""

__author__ = """Bruno Dupuis"""
__email__ = 'lisael@lisael.org'
__version__ = '0.1.0'
