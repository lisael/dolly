import asyncio

import asyncpg

__all__ = ['transaction']


class Database:
    registry = {}

    def __init__(self, name=None, **con_args):
        # TODO: add pool args
        self.name = name
        self.con_args = con_args
        self.__class__.registry[name] = self
        self._pool = None

    async def init(self):
        if self._pool is None:
            self._pool = await asyncpg.create_pool(**self.con_args, loop=asyncio.get_event_loop())

    def connection(self):
        return self._pool.acquire()

    @classmethod
    def get(cls, name):
        return cls.registry[name]


def __getattr__(name):
    return Database.registry[name]


class Transaction():

    def __init__(self, db="default", isolation="read_committed", readonly=False,
                 deferrable=False, con=None):
        self.db = db
        self.trn = None
        self.con = con
        self.isolation = isolation
        self.readonly = readonly
        self.deferrable = deferrable

    async def __aenter__(self):
        if self.con is None:
            self.con = await Database.registry[self.db].connection()
        self.trn = self.con.transaction(isolation=self.isolation,
                                        readonly=self.readonly,
                                        deferrable=self.deferrable)
        await self.trn.__aenter__()
        return self

    async def __aexit__(self,  exc_type, exc_value, traceback):
        return await self.trn.__aexit__(exc_type, exc_value, traceback)

    def commit(self):
        return self.trn.commit()

    def rollback(self):
        return self.trn.rollback()

    def transaction(self, isolation="read_committed", readonly=False,
                    deferrable=False):
        return Transaction(isolation=isolation, readonly=readonly,
                           deferrable=deferrable, con=self.con)

    def fetch(self, *args, **kwargs):
        return self.con.fetch(*args, **kwargs)

    def fetchrow(self, *args, **kwargs):
        return self.con.fetchrow(*args, **kwargs)

    def fetchval(self, *args, **kwargs):
        return self.con.fetchval(*args, **kwargs)

    def execute(self, *args, **kwargs):
        return self.con.execute(*args, **kwargs)

    async def alter(self, *args, **kwargs):
        result = await self.con.execute(*args, **kwargs)
        await self.con.reload_schema_state()
        return result


transaction = Transaction
