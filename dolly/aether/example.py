import asyncio
import random

from dolly.aether import Aether, AetherValue, AetherRef, requires


class RequestAether(Aether):
    request = AetherValue()
    reponse = AetherValue()

    def __init__(self, req):
        super().__init__()
        self.request = req


class OddAether(Aether):
    request = AetherRef(RequestAether)

    @requires("odd_stuff")
    async def __call__(self):
        await asyncio.sleep(random.random())
        self.request.response = "Odd %s %s %s" % (
            self.request.request, await self.odd_stuff, await self.odd_stuff)

    @AetherValue
    async def odd_stuff(self):
        print("compute stuff...")
        await asyncio.sleep(random.random())
        return "odd stuff"


class EvenAether(Aether):
    request = AetherRef(RequestAether)
    async def __call__(self):
        await asyncio.sleep(random.random())
        self.request.response = "Even %s" % self.request.request


class RouterAether(Aether):
    request = AetherRef(RequestAether)
    async def __call__(self):
        req = self.request
        if req.request % 2:
            handler = OddAether()
        else:
            handler = EvenAether()
        await handler()
        print(req.request, req.response)


async def main():
    tasks = []
    for i in range(10):
        RequestAether(i)
        tasks.append(asyncio.create_task(RouterAether()()))
    await asyncio.gather(*tasks)
    print("done")

asyncio.run(main())
