import asyncio
from contextvars import ContextVar

"""
The concept of aether wraps python contextvars to provide
a class that can access the current context easyly. It also
provides lazy async attributes that evaluation can be forced
ahead of time (allowing parallization of database queries).

Read .example for a complete example.
"""


class _AetherMeta(type):
    registry = {}

    def __new__(cls, name, bases, attrs):
        new = super().__new__(cls, name, bases, attrs)
        aether_name = ".".join([attrs["__module__"], name])
        cls.registry[aether_name] = ContextVar(aether_name)
        return new


class Aether(metaclass=_AetherMeta):

    def __init__(self):
        self.__aether_tasks__ = {}
        _AetherMeta.registry[self.__class__._ather_name()].set(self)

    @classmethod
    def _ather_name(cls):
        return ".".join([cls.__module__, cls.__name__])


class AetherValue:
    def __init__(self, func=None):
        self.func = func
        self.name = None
        self.token = None

    def __get__(self, instance, owner):
        if instance is None:
            return self
        if self.func is not None:
            try:
                task = instance.__aether_tasks__[self.name]
            except KeyError:
                task = asyncio.create_task(self.func(instance))
                instance.__aether_tasks__[self.name] = task
            return task
        return self.token.get()

    def __set__(self, instance, value):
        
        self.token.set(value)

    def __set_name__(self, owner, name):
        self.name = name
        self.token = ContextVar(name)


class AetherRef:
    def __init__(self, aether_class):
        if isinstance(aether_class, str):
            self.aether_name = aether_class
        else:
            self.aether_name = ".".join(
                [aether_class.__module__, aether_class.__name__])

    def __get__(self, instance, owner):
        if instance is None:
            return self
        return _AetherMeta.registry[self.aether_name].get()


class requires:
    def __init__(self, name):
        self.name = name

    def __call__(self, fn):
        def wrapper(instance):
            getattr(instance, self.name)
            return fn(instance)
        return wrapper
