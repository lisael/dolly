import click
from IPython import embed
from IPython.core.interactiveshell import _asyncio_runner
from traitlets.config import get_config

from dolly.cli import Command
from dolly.conf.setup import setup, app_registry


class Shell(Command):
    """
    Open an IPython shell
    """

    def __call__(self):
        setup()
        c = get_config()
        c.InteractiveShellEmbed.colors = "Neutral"
        for app in app_registry.values():
            for cls in app.model_classes:
                locals()[cls.__name__] = cls
        embed(config=c, using=None)
