class dolly:
    apps = [
        "dolly.contrib.auth",
        "dolly.contrib.db",
    ]


class database:
    databases = {
        "admin": {},
        "default": {
            "database": "dolly"
        }
    }
