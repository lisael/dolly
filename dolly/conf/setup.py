import threading
import importlib

from dolly.database import Database

current_app = threading.local()

from dolly.orm.models import Model

_setup_done = False


class App:
    registry = {}

    def __init__(self, name, module, models, settings, commands):
        self.name = name
        self.module = module
        self.models = models
        self.model_classes = [v for v in models.__dict__.values() if isinstance(v, type) and issubclass(v, Model)]
        self.model_names = [cls.__name__ for cls in self.model_classes]
        self.settings = settings
        self.commands = commands
        self.__class__.registry.setdefault(name, self)

    def getmodel(self, name):
        self.models.__dict__[name]


app_registry = App.registry


def setup_app(mod):
    module = importlib.import_module(mod)
    current_app.app = module.__dolly_appname__
    models = importlib.import_module(mod + ".models")
    settings = importlib.import_module(mod + ".settings")
    commands = importlib.import_module(mod + ".commands")
    App(module.__dolly_appname__, module, models, settings, commands)


def setup_apps(mods):
    for mod in mods:
        setup_app(mod)


def setup_db(db):
    for k, kwargs in db.databases.items():
        Database(name=k, **kwargs)

def setup():
    if _setup_done:
        return
    from .settings import dolly
    setup_apps(dolly.apps)
    from .settings import database
    setup_db(database)
