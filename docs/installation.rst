.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Dolly, run this command in your terminal:

.. code-block:: console

    $ pip install dolly

This is the preferred method to install Dolly, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Dolly can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://github.com/lisael/dolly

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://github.com/lisael/dolly/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://github.com/lisael/dolly
.. _tarball: https://github.com/lisael/dolly/tarball/master
